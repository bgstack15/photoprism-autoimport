#!/bin/sh
# File: autoimport.sh
# Location: https://gitlab.com/bgstack15/photoprism-autoimport/
# Author: bgstack15
# SPDX-License-Identifier: GPL-3.0
# Startdate: 2022-05-10 13:14
# Title: Start Import Job for PhotoPrism from CLI
# Purpose: with a single shell script, kick off an import process for a named import directory in PhotoPrism
# History:
# Usage:
#    You can dot-source this script to get the functions.
#    You can also just call the script by itself, with an optional env var CONFFILE.
#    Conffile must contain dot-source script that contains variables: username password url directory
# Dependencies:
#    CONFFILE
#    req-devuan: jq
# References:
#    LW devtools
#    https://docs.photoprism.app/developer-guide/ui/rest-api/
#    https://pkg.go.dev/github.com/photoprism/photoprism/internal/api?utm_source=godoc#StartImport
#    https://stackoverflow.com/questions/2683279/how-to-detect-if-a-script-is-being-sourced/28776166#28776166

workdir="$( dirname "$( readlink -f "${0}" 2>/dev/null )" 2>/dev/null || echo "${PWD}" )"
#echo "workdir=${workdir}"
test -z "${CONFFILE}" && CONFFILE="${workdir}/photoprism-autoimport.conf"
test -e "${CONFFILE}" && . "${CONFFILE}"
test -z "${LOGFILE}" && LOGFILE="${workdir}/autoimport.log"

new_session() {
   # dependencies: CONFFILE is dot-sourced already
   # call: new_session
   # get session
   result="$( curl --silent -L "${url%%/}/api/v1/session" -X POST -H 'Content-Type: application/json' --data "{\"username\":\"${username}\",\"password\":\"${password}\"}" )"
   export SESSION="$( echo "${result}" | jq --raw-output '.id' )"
   #echo "inside function, session=${session}"
}

import_directory() {
   # dependencies: SESSION
   # call: import_directory "${DIRECTORY}"
   ___id_dir="${1}"
   test -z "${SESSION}" && new_session # sets env var SESSION
   echo "curl --silent ${max_time:+--max-time ${max_time} }-L \"${url%%/}/api/v1/import\" -X POST --data \"{\\\"path\\\":\\\"${___id_dir}\\\"}\" -H \"X-Session-ID: ${SESSION}\""
   curl --silent ${max_time:+--max-time ${max_time} }-L "${url%%/}/api/v1/import" -X POST --data "{\"path\":\"${___id_dir}\"}" -H "X-Session-ID: ${SESSION}"
}

list_directories() {
   # dependencies: SESSION
   # goal: list available directories for running an import task
   test -z "${SESSION}" && new_session
   #curl --silent "${url%%/}/api/v1/folders/import" -H "X-Session-ID: ${SESSION}" | jq '.folders[].Root+"/"+.folders[].Path'
   curl --silent "${url%%/}/api/v1/folders/import" -H "X-Session-ID: ${SESSION}" | jq '.folders[].Path'
}

main() {
   #echo "outside function, session=${session}"
   import_directory "${directory}"
}

# Determine if this script was dot-sourced

sourced=0
if [ -n "$ZSH_EVAL_CONTEXT" ]; then 
  case $ZSH_EVAL_CONTEXT in *:file) sourced=1;; esac
elif [ -n "$KSH_VERSION" ]; then
  [ "$(cd $(dirname -- $0) && pwd -P)/$(basename -- $0)" != "$(cd $(dirname -- ${.sh.file}) && pwd -P)/$(basename -- ${.sh.file})" ] && sourced=1
elif [ -n "$BASH_VERSION" ]; then
  (return 0 2>/dev/null) && sourced=1 
else # All other shells: examine $0 for known shell binary filenames
  # Detects `sh` and `dash`; add additional shell filenames as needed.
  case ${0##*/} in sh|dash) sourced=1;; esac
fi

# So, if not dot-sourced, and this is run by cron, add logging
if test $sourced -eq 0;
then
   if echo " ${@} " | grep -q cron ;
   then 
      main 2>&1 | plecho | tee -a "${LOGFILE}"
      printf '\n' | tee -a "${LOGFILE}"
   else
      main
   fi
fi
:
