# Readme for photoprism-autoimport

## Overview
Photoprism-autoimport is a small project that uses the API of [PhotoPrism] to initiate a new import of a named directory from the command line.

## Reason for existence
This is useful when the photos to be imported are added in a manner that does not already trigger photoprism's import task. When you use webdav to upload files to an import directory for photoprism, the app already triggers an import task so many seconds afterwards. However, I use a different method ([SyncThing](https://syncthing.net)) so this autoimport project makes my life easier.

## Alternatives

* Use PhotoSync android app. I don't use this because it's not listed in F-droid.
* Use some other WebDAV upload ability.
* Manually trigger uploads in PhotoPrism interface with heading Library -> Import tab.
* Use a [docker-compose file](https://dl.photoprism.app/docker/scheduler/) that includes a job scheduler that runs `photoprism import` inside the docker container. Linked from [Reference 4][4].

## PhotoPrism-autoimport upstream
[This repository](https://gitlab.com/bgstack15/photoprism-autoimport/) is the upstream source of this project.

## Dependencies

* curl
* jq
* POSIX shell (bash, ksh, dash, etc.)
* `photoprism-autoimport.conf` configured like the example. This file contains a dot-sourceable shell script that defines variables:
  * username
  * password
  * url
  * directory (short path, not "/import/Pathname"). Case-sensitive.

## Using
Call the shell script, after configuring the config file. Because the API request is synchronous, a long import task might cause curl to fail out. The import task is still operating however. If you anticipate long import jobs, set the `max_time` value in the config file.

## References
[1]: <https://docs.photoprism.app/developer-guide/ui/rest-api/> REST API - PhotoPrism

[2]: <https://pkg.go.dev/github.com/photoprism/photoprism/internal/api?utm_source=godoc#StartImport> api package - github.com/photoprism/photoprism/internal/api - pkg.go.dev

[3]: <https://stackoverflow.com/questions/2683279/how-to-detect-if-a-script-is-being-sourced/28776166#28776166> bash - How to detect if a script is being sourced - Stack Overflow

[4]: <https://docs.photoprism.app/getting-started/docker-compose/> Docker Compose - PhotoPrism
